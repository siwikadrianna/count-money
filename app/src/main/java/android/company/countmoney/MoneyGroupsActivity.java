package android.company.countmoney;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MoneyGroupsActivity extends AppCompatActivity {
    private static final String TAG = ".MoneyGroupsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lvMoneyGroups = findViewById(R.id.lvMoneyGroups);
        ArrayList<MoneyGroups> moneyGroupsArray = new ArrayList<MoneyGroups>();
        MoneyGroupsAdapter adapter = new MoneyGroupsAdapter(this, R.layout.adapter_money_group, moneyGroupsArray);
        lvMoneyGroups.setAdapter(adapter);
        Button buttonPlus = findViewById(R.id.btAdd);

        buttonPlus.setOnClickListener(v -> {
            Log.d(TAG, "onClick: opening dialog.");
            AddDialog dialog = new AddDialog();
            dialog.show(getSupportFragmentManager(), "AddDialog");
        });
    }
}