package android.company.countmoney;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class MoneyGroupsAdapter extends ArrayAdapter<MoneyGroups> {
    private static final String Tag = ".MoneyGroupsAdapter";
    private Context mContext;
    int mResources;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String moneyGroupsName = getItem(position).getName();
        String moneyGroupsDescription = getItem(position).getDescription();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResources, parent, false);
        TextView tvName = convertView.findViewById(R.id.tvName);
        TextView tvDescription = convertView.findViewById(R.id.tvDescription);

        tvName.setText(moneyGroupsName);
        tvDescription.setText(moneyGroupsDescription);
        return convertView;
    }

    public MoneyGroupsAdapter(Context context, int resource, ArrayList<MoneyGroups> objects) {
        super(context, resource, objects);
        mContext = context;
        mResources = resource;
    }

}